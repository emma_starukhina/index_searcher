package org.starukhina.test_project;

import org.starukhina.file_indexer.service.Indexer;
import org.starukhina.file_indexer.service.IndexerFactory;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import static org.starukhina.test_project.FileSearch.Commands.*;

public class FileSearch {

    class Commands {
        static final String ADD = "add";
        static final String REMOVE = "remove";
        static final String SEARCH = "search";
        static final String HELP = "help";
        static final String EXIT = "exit";
    }

    public static void main(String[] args) {
        Indexer indexer = IndexerFactory.createIndexer(true);
        Scanner in = new Scanner(System.in);
        while (true) {
            try {
                String[] command = in.nextLine().split(" ");
                if(command.length < 1)
                    break;
                String commandName = command[0];
                String argument = command.length < 2 ? "" : command[1];

                switch (commandName.toLowerCase()) {
                    case ADD:
                        addPathToIndex(indexer, argument);
                        break;
                    case REMOVE:
                        removePathFromIndex(indexer, argument);
                        break;
                    case SEARCH:
                        searchWord(indexer, argument);
                        break;
                    case HELP:
                        printHelp();
                        break;
                    case EXIT:
                        return;
                    default:
                        printHelp();
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    private static void addPathToIndex(Indexer indexer, String pathString) throws ExecutionException, InterruptedException {
        if (pathString.isEmpty()) {
            System.out.println("Error. Path argument is empty!");
            return;
        }

        Path path = Paths.get(pathString);
        if (!Files.exists(path)) {
            System.out.println("Path does not exist");
            return;
        }
        indexer.addToIndex(path).get();
        System.out.println("Path was added to index");
    }

    private static void removePathFromIndex(Indexer indexer, String pathString) throws ExecutionException, InterruptedException {
        if (pathString.isEmpty()) {
            System.out.println("Error. Path argument is empty!");
            return;
        }
        Path path = Paths.get(pathString);
        if (!Files.exists(path)) {
            System.out.println("Path does not exist");
            return;
        }
        indexer.removeFromIndex(Paths.get(pathString)).get();
        System.out.println("Path was removed from index");
    }

    private static void searchWord(Indexer indexer, String word) {
        if (word.isEmpty()) {
            System.out.println("Error. Word argument is empty!");
            return;
        }
        Set<Path> paths = indexer.search(word);
        if (!paths.isEmpty()) {
            System.out.println("Word: " + word + " was found in following files: ");
            paths.forEach(System.out::println);

            return;
        }
        System.out.println("Word: " + word + " was not found in indexed directories.");
    }

    private static void printHelp() {
        System.out.println("Supporting commands: ");
        System.out.println("    add     'path_argument'     - add path of file or directory in indexer (recursively)");
        System.out.println("    remove  'path_argument'     - remove path of file or directory in indexer (recursively)");
        System.out.println("    search  'word_argument'     - search word in indexing files");
        System.out.println("    help");
        System.out.println("    exit ");
    }
}
